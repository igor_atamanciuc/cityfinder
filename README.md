# CityFinder#

CityFinder is a simple app for searching cities fast. When you tap on a city it opens the map with that city.

## Requirements
- Xcode 12.2
- Swift 5
- iOS 13.0+

## How to run

- Open in any simulator and start searching


