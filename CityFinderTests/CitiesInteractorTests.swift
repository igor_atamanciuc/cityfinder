import XCTest
import Foundation
@testable import CityFinder

class CitiesInteractorTests: XCTestCase {
    var interactor: CitiesInteractor!
    var mockCitiesView: MockCitiesView!
    var mockCitiesLoader: MockCitiesLoader!
    
    override func setUp() {
        super.setUp()
        mockCitiesView = MockCitiesView()
        mockCitiesLoader = MockCitiesLoader()
        
        interactor = CitiesInteractor(view: mockCitiesView, citiesLoader: mockCitiesLoader)
    }
    
    
    func test_viewDidLoad_shouldTriggerTheLoadingOfTheCities() {
        // GIVEN
        
        // WHEN
        interactor.viewDidLoad()
        
        // THEN
        XCTAssertTrue(mockCitiesLoader.loadCitiesWasCalled)
    }
    
    func test_viewDidLoad_whenCitiesAreLoaded_shouldReturnTheCorrectCity() {
        // GIVEN
        let expectedCities = [mockedCity(country: "UA", name: "Test 1"), mockedCity(country: "AU", name: "Test 2")]
        mockCitiesLoader.citiesStub = expectedCities
        
        // WHEN
        interactor.viewDidLoad()
        
        // THEN
        XCTAssertEqual(interactor.item(for: 0), expectedCities[0])
        XCTAssertEqual(interactor.item(for: 1), expectedCities[1])
    }
    
    func test_viewDidLoad_afterLoadingCities_shouldUpdateViewState() {
        // GIVEN
        let expectedCities = [mockedCity(country: "UA", name: "Test 1"), mockedCity(country: "AU", name: "Test 2")]
        mockCitiesLoader.citiesStub = expectedCities
        
        // WHEN
        interactor.viewDidLoad()
        
        // THEN
        XCTAssertEqual(mockCitiesView.stateStub, CitiesViewState.reload)
    }
    
    func test_viewDidLoad_afterLoading_citiesShouldBeSortedAlphabetically() {
        
        let expectedCities = [mockedCity(country: "UA", name: "Chisinau"), mockedCity(country: "AU", name: "Apaca")]
        mockCitiesLoader.citiesStub = expectedCities
        let sortedCities = expectedCities.sorted(by: {$0.name < $1.name })
        
        // WHEN
        interactor.viewDidLoad()
        
        // THEN
        XCTAssertEqual(interactor.item(for: 0), sortedCities[0])
    }
    
    private func mockedCity(country: String,
                            name: String) -> City {
        return City(country: country,
                    name: name,
                    id: 0000,
                    coordinates: City.Coordinates(lon: 0.0, lat: 1.1))
    }
}


