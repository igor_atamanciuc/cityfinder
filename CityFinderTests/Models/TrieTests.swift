import XCTest
@testable import CityFinder

class TrieTests: XCTestCase {
    private var citiesTrie = Trie<City>()
    
    override func setUp() {
        super.setUp()
        
        citiesTrie.insert(mockedCity(country: "MD", name: "Chisinay"))
        citiesTrie.insert(mockedCity(country: "US", name: "Orhe"))
        citiesTrie.insert(mockedCity(country: "US", name: "Chisinaa"))
        citiesTrie.insert(mockedCity(country: "AA", name: "Amster"))
    }

    func test_trieSearchResultWithValidPrefix_shouldReturnMatchedValues() {
        // GIVEN
        
        // WHEN
        let cities = Set(citiesTrie.find(prefix: "Chisi".lowercased()))
        
        // THEN
        let expectedResult = Set([mockedCity(country: "MD", name: "Chisinay"),
                               mockedCity(country: "US", name: "Chisinaa")])
        XCTAssertEqual(expectedResult, cities)
    }
    
    func test_trieSearchResultWithValidPrefix_shouldReturnOneMatchedValues() {
        // GIVEN
        
        // WHEN
        let cities = Set(citiesTrie.find(prefix: "Chisinaa".lowercased()))
        
        // THEN
        let expectedResult = Set([mockedCity(country: "US", name: "Chisinaa")])
        XCTAssertEqual(expectedResult, cities)
    }
    
    func test_trieSearchResultWithInvalidPrefix_shouldReturnEmptyResult() {
        // GIVEN

        // WHEN
        let cities = citiesTrie.find(prefix: "ccccc")
        
        // THEN
        XCTAssertTrue(cities.isEmpty)
    }
    
    func test_TrieSearchResultWithEmptyPrefix_shouldReturnEmptyResults() {
        // WHEN
        let cities = citiesTrie.find(prefix: "")
        
        // THEN
        XCTAssertTrue(cities.isEmpty)
    }
    
    private func mockedCity(country: String,
                            name: String) -> City {
        return City(country: country,
                    name: name,
                    id: 0000,
                    coordinates: City.Coordinates(lon: 0.0, lat: 1.1))
    }
}
