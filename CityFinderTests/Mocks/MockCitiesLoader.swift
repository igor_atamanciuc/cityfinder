import Foundation
@testable import CityFinder

final class MockCitiesLoader: CitiesLoaderType {
    var citiesStub: [City]?
    var errorStub: FileLoaderError?
    var loadCitiesWasCalled = false
    func loadCities(from resource: FileResource, completion: @escaping (Result<[City], FileLoaderError>) -> Void) {
        loadCitiesWasCalled = true
        if let error = errorStub {
            completion(.failure(error))
        } else if let cities = citiesStub {
            completion(.success(cities))
        }
    }
}
