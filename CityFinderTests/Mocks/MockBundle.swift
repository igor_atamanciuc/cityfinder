import Foundation
@testable import CityFinder

final class MockBundle: BundleType {
    var mockURLStub: URL? = nil
    func url(forResource name: String?, withExtension ext: String?) -> URL? {
        return mockURLStub
    }
}
