import Foundation
@testable import CityFinder

final class MockCitiesView: CitiesViewInput {
    var stateStub: CitiesViewState?
    func setState(_ state: CitiesViewState) {
        stateStub = state
    }
    
    var cityStub: City?
    func showCityOnMap(city: City) {
        cityStub = city
    }
}
