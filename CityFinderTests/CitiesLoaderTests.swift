import XCTest
@testable import CityFinder

class CitiesLoaderTests: XCTestCase {
    var citiesLoader: CitiesLoader!
    
    func test_loadCities_whenURLisInvalid_shouldReturnInvalidPathError() {
        // GIVEN
        citiesLoader = CitiesLoader(bundle: MockBundle())
        let resource = FileResource(path: "no_cities", type: "test")
        let expectation = self.expectation(description: "Loader should return an error")
        var expectedResult: Result<[City], FileLoaderError>?
        
        // WHEN
        citiesLoader.loadCities(from: resource) { result in
            expectedResult = result
            expectation.fulfill()
        }
        
        // THEN
        waitForExpectations(timeout: 5, handler: nil)
        switch expectedResult {
        case .none: XCTFail("Expected to fail because of invalid URL")
        case .success:
            XCTFail("Expected to fail because of invalid URL")
        case .failure(let error):
            guard case .invalidPath(let message) = error else {
                return XCTFail("Expected to fail because of invalid URL")
            }
            XCTAssertEqual(message, "no_cities.test")
        }
    }
    
    func test_loadCities_whenURLisValid_shouldReturnCities() {
        // GIVEN
        let bundle = Bundle(for: CitiesLoaderTests.self)
        citiesLoader = CitiesLoader(bundle: bundle)
        let resource = FileResource(path: "test_cities", type: "json")
        let expectation = self.expectation(description: "Loader should return an error")
        var expectedResult: Result<[City], FileLoaderError>?
        let expectedCities = Set([City(country: "UA",
                                       name: "Hurzuf",
                                       id: 707860,
                                       coordinates: .init(lon: 34.283333, lat: 44.549999)),
                                  City(country: "RU",
                                       name: "Novinki",
                                       id: 519188,
                                       coordinates: .init(lon: 37.666668, lat: 55.683334))])
        
        // WHEN
        citiesLoader.loadCities(from: resource) { result in
            expectedResult = result
            expectation.fulfill()
        }
        
        // THEN
        waitForExpectations(timeout: 5, handler: nil)
        switch expectedResult {
        case .none: XCTFail("Expected to load the cities from the json file")
        case .success(let cities):
            XCTAssertEqual(expectedCities, Set(cities))
        case .failure(_):
            return XCTFail("Expected to load the cities from the json file")
        }
    }
    
    func test_loadCities_whenJSONProvidesBadData_shouldReturAnError() {
        // GIVEN
        let bundle = Bundle(for: CitiesLoaderTests.self)
        citiesLoader = CitiesLoader(bundle: bundle)
        let resource = FileResource(path: "test_bad_cities", type: "json")
        let expectation = self.expectation(description: "Loader should return an error")
        var expectedResult: Result<[City], FileLoaderError>?
        
        // WHEN
        citiesLoader.loadCities(from: resource) { result in
            expectedResult = result
            expectation.fulfill()
        }
        
        // THEN
        waitForExpectations(timeout: 5, handler: nil)
        switch expectedResult {
        case .none: XCTFail("Expected to return a decoding error")
        case .success(_):
            XCTFail("Expected to return a decoding error")
        case .failure(let error):
            guard case .undefined(let decodingError) = error else {
                return XCTFail("Expected to return a decoding error")
            }
            XCTAssert(decodingError is DecodingError)
        }
    }
}


