protocol CitiesLoaderType {
    func loadCities(from resource: FileResource, completion: @escaping (Result<[City], FileLoaderError>) -> Void)
}

enum FileLoaderError: Error {
    case invalidPath(String)
    case undefined(Error)
}
