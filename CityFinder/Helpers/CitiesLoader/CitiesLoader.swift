import Foundation

struct CitiesLoader: CitiesLoaderType {
    let queue = DispatchQueue(label: "com.iatamanciuc.cities", qos: .utility)
    let bundle: BundleType
    
    init(bundle: BundleType) {
        self.bundle = bundle
    }
    
    func loadCities(from resource: FileResource, completion: @escaping (Result<[City], FileLoaderError>) -> Void) {
        queue.async {
            guard let url = bundle.url(forResource: resource.path, withExtension: resource.type) else {
                completion(.failure(.invalidPath(resource.fullPath)))
                return
            }
            
            do {
                let data = try Data(contentsOf: url)
                let cities = try JSONDecoder().decode([City].self, from: data)
                completion(.success(cities))
            } catch {
                completion(.failure(.undefined(error)))
            }
        }
    }
}
