enum Constants {
    enum Cities {
        static let title = "City Finder"
        static let loadingCities = "Loading Cities"
        static let noResults = "No cities were found, please check your input"
        
    }
    
    enum Alerts {
        static let error = "Error"
        static let somethingWentWrong = "Something went wrong."
        static let retry = "Retry"
    }
    enum Files {
        static let name = "cities"
        static let json = "json"
    }
}
