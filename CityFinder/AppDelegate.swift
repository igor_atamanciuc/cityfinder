//
//  AppDelegate.swift
//  CityFinder
//
//  Created by Igor Atamanciuc on 03/12/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: CitiesBuilder.build())
        window?.makeKeyAndVisible()
        
        return true
    }
}
