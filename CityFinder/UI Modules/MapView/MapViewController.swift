import UIKit
import MapKit

final class MapViewController: UIViewController, StoryboardInstantiable {
    static var storyboardName = "Map"
    
    @IBOutlet private weak var mapView: MKMapView!
    
    var interactor: MapViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapView()
        interactor.viewDidLoad()
    }
    
    private func setupMapView() {
        mapView.delegate = self
    }
}

extension MapViewController: MapViewInput {
    func showCityLocation(long: Double, lat: Double) {
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let radius: CLLocationDistance = 10000
        let coordinateRegion = MKCoordinateRegion(
             center: coordinate,
             latitudinalMeters: radius,
             longitudinalMeters: radius)
        mapView.setRegion(coordinateRegion, animated: true)
    
    }
    
    func showCityPin(_ pin: MKAnnotation) {
        mapView.addAnnotation(pin)
    }
    
    func setTitle(_ cityTitle: String) {
        title = cityTitle
    }    
}

extension MapViewController: MKMapViewDelegate {
    
}
