import UIKit

struct MapViewBuilder {
    static func build(with city: City) -> UIViewController {
        
        let mapViewController = MapViewController.instantiate()
        let interactor = MapViewInteractor(view: mapViewController, city: city)
        mapViewController.interactor = interactor
        
        return mapViewController
    }
}
