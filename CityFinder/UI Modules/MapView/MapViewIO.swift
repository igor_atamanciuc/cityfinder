import Foundation
import MapKit

protocol MapViewInput: AnyObject {
    func showCityLocation(long: Double, lat: Double)
    func showCityPin(_ pin: MKAnnotation)
    func setTitle(_ cityTitle: String)
}

protocol MapViewOutput: AnyObject {
    func viewDidLoad()
}
