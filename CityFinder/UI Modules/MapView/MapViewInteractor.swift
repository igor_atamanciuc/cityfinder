import Foundation

final class MapViewInteractor {
    private weak var view: MapViewInput?
    
    private let city: City
    
    init(view: MapViewInput, city: City) {
        self.view = view
        self.city = city
    }
}

extension MapViewInteractor: MapViewOutput {
    func viewDidLoad() {
        view?.showCityLocation(long: city.coordinates.lon, lat: city.coordinates.lat)
        view?.showCityPin(CityAnnotation(city: city))
        view?.setTitle(city.name)
    }
}
