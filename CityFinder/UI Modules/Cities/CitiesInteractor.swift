import Foundation
import UIKit

final class CitiesInteractor {
    private weak var view: CitiesViewInput!
    private var citiesLoader: CitiesLoaderType
    
    private var cities: [City] = []
    private var citiesTrie = Trie<City>()
    private var filteredCities: [City] = []
    
    
    init(view: CitiesViewInput, citiesLoader: CitiesLoaderType) {
        self.view = view
        self.citiesLoader = citiesLoader
    }
    
    // MARK:- Private methods
    private func loadCities() {
        let citiesFileResource = FileResource(path: Constants.Files.name, type: Constants.Files.json)
        citiesLoader.loadCities(from: citiesFileResource) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let cities): self.completeCitiesLoading(allCities: cities)
            case .failure(let error): self.completeCitiesLoadingWithError(error, retry: { self.loadCities() })
            }
        }
    }
    
    private func completeCitiesLoading(allCities: [City]) {
        cities = allCities.sorted(by: {$0.name < $1.name })
        filteredCities = cities
        cities.forEach(citiesTrie.insert)
        self.view.setState(.reload)
    }
    
    private func completeCitiesLoadingWithError(_ error: FileLoaderError, retry: @escaping () -> Void) {
        let retryAction = UIAlertAction(title: Constants.Alerts.retry, style: .cancel, handler: { _ in retry() })
        self.view.setState(.failed(title: Constants.Alerts.error,
                                   message: Constants.Alerts.somethingWentWrong,
                                   actions: [retryAction]))
    }
}

extension CitiesInteractor: CitiesViewOutput {
    func viewDidLoad() {
        view.setState(.loading)
        loadCities()
    }
    
    func searchTextDidChange(with text: String) {
        filteredCities = text.isEmpty ? cities : citiesTrie.find(prefix: text.lowercased())
        filteredCities.isEmpty ? view.setState(.noResults) : view.setState(.reload)
    }
    
    func didTapOnItem(at index: Int) {
        guard index < filteredCities.count else { return }
        let city = filteredCities[index]
        view?.showCityOnMap(city: city) // Because the app is super simple with only 2 screens the view layer is allowed to present another module. However a better approach for the navigation is to have dedicated object that handles the navigation. The coordinator pattern is a good candidate.
    }
    
    var citiesCount: Int {
        return filteredCities.count
    }
    
    func item(for index: Int) -> City? {
        guard index < filteredCities.count else { return nil }
        return filteredCities[index]
    }
}
