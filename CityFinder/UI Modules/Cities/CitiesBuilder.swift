import UIKit

struct CitiesBuilder {
    static func build() -> UIViewController {
        let viewController = CitiesViewController.instantiate()
        let citiesLoader = CitiesLoader(bundle: Bundle.main)
        let interactor = CitiesInteractor(view: viewController,
                                          citiesLoader: citiesLoader)
        viewController.interactor = interactor
        
        return viewController
    }
}
