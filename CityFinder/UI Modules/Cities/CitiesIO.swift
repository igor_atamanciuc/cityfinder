import UIKit

enum CitiesViewState: Equatable {
    case loading
    case failed(title: String, message: String, actions: [UIAlertAction])
    case reload
    case noResults
}

protocol CitiesViewInput: AnyObject {
    func setState(_ state: CitiesViewState)
    func showCityOnMap(city: City)
}

protocol CitiesViewOutput: AnyObject {
    var citiesCount: Int { get }
    func viewDidLoad()
    func item(for index: Int) -> City?
    func searchTextDidChange(with text: String)
    func didTapOnItem(at index: Int)
}
