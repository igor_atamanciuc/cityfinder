import UIKit

final class CitiesViewController: UIViewController, StoryboardInstantiable  {
    static var storyboardName = "Cities"
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var citiesListTableView: UITableView!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var interactor: CitiesViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        setupNavigationBar()
        setupTableView()
        setupSearchBar()
        citiesListTableView.reloadData()
        interactor.viewDidLoad()
    }
    
    private func setupNavigationBar() {
        title = Constants.Cities.title
    }
    
    private func setupTableView() {
        citiesListTableView.delegate = self
        citiesListTableView.dataSource = self
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
    }
}

extension CitiesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.citiesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        guard let city = interactor.item(for: indexPath.row) else { return cell }
        cell.textLabel?.text = city.descriptiveName
        cell.detailTextLabel?.text = city.descriptiveCoordinates
        
        return cell
    }
}

extension CitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        interactor.didTapOnItem(at: indexPath.row)
    }
}

extension CitiesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        interactor.searchTextDidChange(with: searchText)
    }
}

extension CitiesViewController: CitiesViewInput {
    func setState(_ state: CitiesViewState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            switch state {
            case .loading: self.showLoadingState()
            case .reload: self.showCitiesList()
            case .noResults: self.showNoResults()
            case .failed(let title, let message, let actions): self.showAlert(title: title, message: message, actions: actions)
            }
        }
    }
    
    func showCityOnMap(city: City) {
        let viewController = MapViewBuilder.build(with: city)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showLoadingState() {
        citiesListTableView.isHidden = true
        searchBar.isHidden = true
        infoLabel.isHidden = false
        infoLabel.text = Constants.Cities.loadingCities
        activityIndicator.isHidden = false
    }
    
    func showCitiesList() {
        activityIndicator.isHidden = true
        citiesListTableView.isHidden = false
        searchBar.isHidden = false
        citiesListTableView.reloadData()
    }
    
    func showNoResults() {
        activityIndicator.isHidden = true
        citiesListTableView.isHidden = true
        searchBar.isHidden = false
        infoLabel.isHidden = false
        infoLabel.text = Constants.Cities.noResults
    }
    
    func showAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach(alertController.addAction)
        
        self.present(alertController, animated: true)
    }
}
