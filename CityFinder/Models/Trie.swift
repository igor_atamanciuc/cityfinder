import Foundation

protocol TrieRepresentable: Equatable {
    associatedtype Representation: Collection

    var trieRepresentation: Representation { get }
}


/* For a fast search through the cities I choosed the search-oriented Trie data structure which store keys and associated values.
 In our case, the keys are always string and associated values can be anything(ex: City). A Trie is a tree that can have more than 2 children from a node. The time for finding a specific word is O(n*m), where n is the length of the word and m is the number of words that have the same root, which makes this data structure super fast for searching a string in a Trie.
 */
final class Trie<Value: TrieRepresentable> where Value.Representation.Element: Hashable {
    public typealias Node = TrieNode<Value>
    private let root = Node(key: nil, parent: nil)
    
    public init() {}
    
    public func insert(_ collection: Value) {
        var current = root
        
        for element in collection.trieRepresentation {
            if current.children[element] == nil {
                current.children[element] = Node(key: element, parent: current)
            }
            current = current.children[element]!
        }
        
        current.isTerminating = true
        current.value = collection
    }
}

extension Trie where Value.Representation: RangeReplaceableCollection {
    func find(prefix: Value.Representation) -> [Value] {
        if prefix.isEmpty { return [] }
        var current = root
        for element in prefix {
            guard let child = current.children[element] else { return [] }
            current = child
        }

        return match(with: prefix, after: current)
    }

    
    private func match(with prefix: Value.Representation, after node: TrieNode<Value>) -> [Value] {
        var results: [Value] = []
        if node.isTerminating, let value = node.value { results.append(value) }
        
        for child in node.children.values {
            guard let key = child.key else { continue }
            var prefix = prefix
            prefix.append(key)
            results.append(contentsOf: match(with: prefix, after: child))
        }
        
        return results
    }
}

