import Foundation

final class TrieNode<Value: TrieRepresentable> where Value.Representation.Element: Hashable {
    public var key: Value.Representation.Element?
    public weak var parent: TrieNode?
    public var children: [Value.Representation.Element: TrieNode] = [:]
    public var isTerminating = false
    public var value: Value?
    
    public init(key: Value.Representation.Element?, parent: TrieNode?) {
        self.key = key
        self.parent = parent
    }
}
