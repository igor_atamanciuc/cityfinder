import Foundation
import MapKit

struct City: Codable, Hashable {
    struct Coordinates: Codable, Hashable {
        let lon: Double
        let lat: Double
    }
    
    let country: String
    let name: String
    let id: Int
    let coordinates: Coordinates
    
    enum CodingKeys: String, CodingKey {
        case country = "country"
        case name
        case id = "_id"
        case coordinates = "coord"
    }
}

extension City {
    var locationCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: coordinates.lat, longitude: coordinates.lon)
    }
    
    var descriptiveName: String {
        return "\(name), \(country)"
    }
    
    var descriptiveCoordinates: String {
        return "Long:\(coordinates.lon), Lat: \(coordinates.lat)"
    }
}

extension City: TrieRepresentable {
    var trieRepresentation: String {
        return self.name.lowercased()
    }
}
