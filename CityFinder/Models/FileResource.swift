struct FileResource {
    let path: String
    let type: String
    
    var fullPath: String {
        return "\(path).\(type)"
    }
}
