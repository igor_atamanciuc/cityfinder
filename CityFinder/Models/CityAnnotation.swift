import MapKit

final class CityAnnotation: NSObject, MKAnnotation {
    private let city: City
    
    var coordinate: CLLocationCoordinate2D {
        return city.locationCoordinate
    }
    
    var title: String? {
        return city.name
    }
    
    init(city: City) {
        self.city = city
    }
}
